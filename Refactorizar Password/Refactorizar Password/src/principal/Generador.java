package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		String password = "";
		password = opciones(longitud, opcion, password);

		System.out.println(password);
		scanner.close();
	}

	private static String opciones(int longitud, int opcion, String password) {
		switch (opcion) {
		case 1:
			traerletra();
			
			break;
		case 2:
			password = generar_numero(longitud, password);
			break;
		case 3:
			password = obtenerLetraOCaracterEspecial(longitud, password);
			break;
		case 4:
			for (int i = 0; i < longitud; i++) {
				int n;
				n = (int) (Math.random() * 3);
				if (n == 1) {
					char letra4;
					letra4 = LetraAleatorio();
					password += letra4;
				} else if (n == 2) {
					char caracter4;
					caracter4 = caracterAleatorio();
					password += caracterAleatorio();
				} else {
					int numero4;
					numero4 = (int) (Math.random() * 10);
					password += numero4;
				}
			}
			break;
		}
		return password;
	}

	public static String obtenerLetraOCaracterEspecial(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 2);
			if (n == 1) {
				char letra3;
				letra3 = LetraAleatorio();
				password += letra3;
			} else {
				char caracter3;
				caracter3 = caracterAleatorio();
				password += caracter3;
			}
		}
		return password;
	}

	public static String generar_numero(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int numero2;
			numero2 = (int) (Math.random() * 10);
			password += numero2;
		}
		return password;
	}

	private static void traerletra() {
		int longitud = 0;
		for (int i = 0; i < longitud; i++) {
			char letra1;
			letra1 = LetraAleatorio();
			char password = letra1;
		}
		
	}

	private static char LetraAleatorio() {
		return (char) ((Math.random() * 26) + 65);
	}

	private static char caracterAleatorio() {
		return (char) ((Math.random() * 15) + 33);
	}

}