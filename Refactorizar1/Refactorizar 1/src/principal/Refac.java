package principal;
/*
 * Clase para refactorizar

 * Codificacion UTF-8
 * Paquete refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea de de bloque.
 */
// error ha de poner import java.awt.Frame;import java.awt.Graphics;
import java.*;
import java.util.*;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
// la clase ha de estar en mayusculas Refac

public class Refac {
	final static String cad = "Bienvenido al programa";
	public static void main(String[] args) {
		//Una sola declaracion de variable por linea  int a y en la linea de abajo int b.
		
		int a, b;
		//una sola declaracio por linea string caden1 y string cadena2
		String cadena1, cadena2;
		Scanner c = new Scanner(System.in);
		
		System.out.println(cad);
		System.out.println("Introduce tu dni");
		cadena1 = c.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = c.nextLine();
		// los valores de la variable habria que ponerlos arriba  con cada int a y el int b.
		a=7; b=16;
		int numeroc = 25;
		if((a>b)
		    ||(numeroc%5!=0&&(numeroc*3-1)>b/numeroc)){
			System.out.println("Se cumple la condición");
		}
		// se deberia utulizar un metodo final static int numeroc=25 y abajo int numeroc = a+b*numeroc+b/a
		numeroc = a+b*numeroc+b/a;
		// los [] deben ir delante de srting String[]array= new String[7].
		String array[] = new String[7];
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";
		
		recorrer_array(array);
	}
	static void recorrer_array(String vectordestrings[])
	{
		for(int dia=0;dia<7;dia++) 
		{
			System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: "+vectordestrings[dia]);
		}
	}
	
}

