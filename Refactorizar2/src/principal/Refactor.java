package principal;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.io.*;
import java.util.*;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor {

	static Scanner a;
	public static void main(String[] args) 
	{
	a = new Scanner(System.in);
	// int n y abajo int cantidad_maxima_alumnos un delcaracion por linea
	int n, cantidad_maxima_alumnos;
	
	cantidad_maxima_alumnos = 10;
	// los [] delante de int (int[]arrays0new int[10]
	int arrays[] = new int[10];
	for(n=0;n<10;n++)
	{
		System.out.println("Introduce nota media de alumno");
		arrays[n] = a.nextInt();
	}	
	
	System.out.println("El resultado es: " + recorrer_array(arrays));
	
	a.close();
}

	static double recorrer_array(int vector[])
{
	
		double c = 0;
		for(int a=0;a<10;a++) 
		{
			c=c+vector[a];
		}
		return c/10;
	}
	

}
